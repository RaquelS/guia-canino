<?php

ini_set('display_errors', TRUE);

require_once $_SERVER['DOCUMENT_ROOT'] . "/dao/NoticiaDao.php";
require_once $_SERVER['DOCUMENT_ROOT'] . "/entidades/Noticia.php";


switch ($_REQUEST ['action']) {
    case 'cadastrarNoticia':
        if (!isset($_REQUEST['titulo']) || !isset($_REQUEST['conteudo']) || !isset($_REQUEST['resumo'])) {
            header('location: ../cadastrar.php?erro=Titulo e Conteúdo Obrigatorio');
        }
        $noticia = new Noticia();
        $noticia->setTitulo($_REQUEST['titulo']);

        if ((isset($_FILES['imagem']['name']) && ($_FILES['imagem']['error'] == 0))) {
            
            $arquivo_tmp = $_FILES['imagem']['tmp_name'];
            $nome = $_FILES['imagem']['name'];
            $extensao = pathinfo($nome, PATHINFO_EXTENSION);
            $extensao = strtolower($extensao);
            
            if ( strstr ( '.jpg;.jpeg;.gif;.png', $extensao ) ) {
                $novoNome = uniqid(time()) . '.' . $extensao;
                $destino = '../img/ ' . $novoNome;
                
                if (\move_uploaded_file($arquivo_tmp, $destino)) {
                    echo 'Arquivo salvo com sucesso em : <strong>' . $destino . '</strong><br />';
                    echo ' < img src = "' . $destino . '" />';
                    
                } else
                    echo 'Erro ao salvar o arquivo. Aparentemente você não tem permissão de escrita.<br />';
                
            }else 
                echo 'Você poderá enviar apenas arquivos "*.jpg;*.jpeg;*.gif;*.png"<br />';
            
            
        } else {
            $destino = NULL;
        }
        $noticia->setImagem($destino);
        $noticia->setConteudo($_REQUEST['conteudo']);
        $noticia->setResumo($_REQUEST['resumo']);

        $subCat;
        $cat;
        if ($_REQUEST['categoria'] == 1) {
            $cat = 1;
            $subCat = $_REQUEST['categoria'];
        } else if ($_REQUEST['categoria'] == 2) {
            $cat = 2;
            $subCat = $_REQUEST['categoria'];
        } else {
            $cat = $_REQUEST['categoria'];
            $subCat = NULL;
        }
        $noticia->setCategoria($cat);
        $noticia->setSubCategoria($subCat);

        $dao = new NoticiaDao();
        $id = $dao->inserirNoticia($noticia);

        if ($id) {
            echo'<script>alert("Noticia adicionada com sucesso")</script>';
            header('location: ../cadastrar.php');
            
        } 

        break;
        
  
    default:
        echo'<script>alert("Opção incorreta")</script>';
        break;
}

    