<html>

    <head>
        <?php
        $title = "Guia Canino - Inserir Notícia";
        include './head.php';
        ?>

    </head>

    <body>

        <?php
        include './header.php';
        ?>

        <div class="container cadastroNoticia">

            <div class="row">
                <h2 class="tituloPagina">Adicionar Notícia</h2>
                <hr class="divisoriaTema">
            </div>

            <form class="group"  
                  action="controller/noticiaController.php?action=cadastrarNoticia"
                  method="POST" enctype="multipart/form-data" >
                <br>
                <label for="titulo">Título</label>
                <input class="form-control" type="text" name="titulo" required="">
                <br>
                <label for="imagem">Escolha a imagem</label>
                <input type="file" name="imagem">
                <br>
                <label for="categoria">Categoria</label>
                <select class="form-control" name="categoria" required="">
                    <option value="1">Alimentação</option>
                    <option value="2">Adestramento</option>
                </select>
                <br>
                <label for="resumo">Resumo</label>
                <textarea class="form-control" name="resumo" rows="5" required=""></textarea>
                <br>
                <label for="conteudo">Conteudo</label>
                <textarea class="form-control" id="summernote" rows="20" name="conteudo" required=""></textarea>

                <button class ="btn btn-lg btn-default" type="submit">Cadastrar</button>
            </form>
        </div>

        <?php
        include './footer.php';
        ?>

    </body>

</html>

