<?php

class Noticia {

    private $titulo;
    private $categoria;
    private $subCategoria;
    private $conteudo;
    private $imagem;
    private $dataPublicacao;
    private $resumo;

    function __construct() {
        
    }

    function getTitulo() {
        return $this->titulo;
    }

    function getCategoria() {
        return $this->categoria;
    }

    function getConteudo() {
        return $this->conteudo;
    }

    function getImagem() {
        return $this->imagem;
    }

    function getSubCategoria() {
        return $this->subCategoria;
    }

    function getResumo() {
        return $this->resumo;
    }

    function setSubCategoria($subCategoria) {
        $this->subCategoria = $subCategoria;
    }

    function getDataPublicacao() {
        return $this->dataPublicacao;
    }

    function setDataPublicacao($dataPublicacao) {
        $this->dataPublicacao = $dataPublicacao;
    }

    function setResumo($resumo) {
        $this->resumo = $resumo;
    }

    function setTitulo($titulo) {
        $this->titulo = $titulo;
    }

    function setCategoria($categoria) {
        $this->categoria = $categoria;
    }

    function setConteudo($conteudo) {
        $this->conteudo = $conteudo;
    }

    function setImagem($imagem) {
        $this->imagem = $imagem;
    }

}
