<footer class="container-fluid text-center footer">
    <div class="pull-left col-md-6 descktop">
        <text class="textoFoter">"O único amigo desinteressado que um homem pode ter neste mundo egoísta é o seu cachorro." 
        <br>(George Graham Vest)</text>
    </div>
    <div class=" col-md-6">
        <div class="pull-right">
            <h6 class="textoFoter">Redes Sociais:</h6>
            <h1 class="iconeFooter"><a style="color: white" href="https://www.facebook.com/" ><i class="fab fa-facebook-square "></i></a></h1>
            <h1 class="iconeFooter"><a style="color: white" href="https://www.instagram.com/"><i class="fab fa-instagram"></i></a></h1>    
        </div>
    </div>
</footer>

<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
<script src="js/bootstrap.min.js" type="text/javascript"></script>
<script src="js/js.js" type="text/javascript"></script>
<script src="summernote-0.8.11-dist/dist/summernote.js" type="text/javascript"></script>
<script src="summernote-0.8.11-dist/dist/summernote.min.js" type="text/javascript"></script>