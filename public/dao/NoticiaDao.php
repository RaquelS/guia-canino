<?php

ini_set('display_errors', TRUE);
require_once $_SERVER['DOCUMENT_ROOT'] . "/util/Conexao.php";

class NoticiaDao {

    public function inserirNoticia(Noticia $noticia) {
        try {
            $sql = 'insert into noticias(titulo,imagem,conteudo,categoria,sub_categoria, data_publicacao,resumo) values(:titulo,:imagem,:conteudo,:categoria,:sub_categoria, now(),:resumo)';
            $p_sql = Conexao::getInstance()->prepare($sql);
            $p_sql->bindValue(':titulo', $noticia->getTitulo());
            $p_sql->bindValue(':imagem', $noticia->getImagem());
            $p_sql->bindValue(':conteudo', $noticia->getConteudo());
            $p_sql->bindValue(':categoria', $noticia->getCategoria());
            $p_sql->bindValue(':sub_categoria', $noticia->getSubCategoria());
            $p_sql->bindValue(':resumo', $noticia->getResumo());

            if ($p_sql->execute()) {
                $id = Conexao::getInstance()->lastInsertId();
                $default = "../defaultNoticia.php";
                $nova_pagina = '../' . $id . '.php';
                copy($default, $nova_pagina);
                $sql = "update noticias set caminho_pagina = '{$nova_pagina}' where id = '{$id}'";
                $p_sql = Conexao::getInstance()->prepare($sql);
                $p_sql->execute();
                return $id;
            }
            return FALSE;
        } catch (Exception $ex) {
            echo '<script>alert("Erro no banco de dados . '.$ex.'")</script>';
        }
    }

    public function buscarNoticiaHome() {
        try {
            $sql = 'select n.id, n.titulo,n.imagem,n.resumo,n.data_publicacao,n.caminho_pagina, c.categoria,s.sub_categoria 
                    from noticias n 
                    left join categoria c on n.categoria = c.id 
                    left join sub_categoria s on n.sub_categoria = s.id
                    order by n.data_publicacao desc
                    limit 5';
            $p_sql = Conexao::getInstance()->prepare($sql);
            $p_sql->execute();
            return $p_sql->fetchAll(PDO::FETCH_ASSOC);
        } catch (Exception $ex) {
           echo '<script>alert("Erro no banco de dados . '.$ex.'")</script>';
        }
    }

    public function buscaNoiciaCategoria($categoria) {
        try {
            $sql = "select n.id, n.titulo,n.imagem,n.resumo,n.data_publicacao, c.categoria 
                    from noticias n 
                    join categoria c on n.categoria = c.id 
                    where n.categoria = '{$categoria}'";
            $p_sql = Conexao::getInstance()->prepare($sql);
            $p_sql->execute();
            return $p_sql->fetchAll(PDO::FETCH_ASSOC);
        } catch (Exception $ex) {
           echo '<script>alert("Erro no banco de dados . '.$ex.'")</script>';
        }
    }

    public function buscaNoticiaSubcategoria($subCategoria) {
        try {
            $sql = "select n.id, n.titulo,n.imagem,n.resumo,n.data_publicacao, s.sub_categoria 
                    from noticias n 
                    join sub_categoria s on n.sub_categoria = s.id 
                    where n.categoria = '{$subCategoria}'";
            $p_sql = Conexao::getInstance()->prepare($sql);
            $p_sql->execute();
            return $p_sql->fetchAll(PDO::FETCH_ASSOC);
        } catch (Exception $ex) {
            echo '<script>alert("Erro no banco de dados . '.$ex.'")</script>';
        }
    }

    public function buscaNoticia($id) {
        try {
            $sql = "select n.id, n.titulo,n.imagem,n.conteudo,n.data_publicacao, c.categoria,s.sub_categoria 
                    from noticias n 
                    left join categoria c on n.categoria = c.id 
                    left join sub_categoria s on n.sub_categoria = s.id 
                    where n.id = '{$id}'";
            $p_sql = Conexao::getInstance()->prepare($sql);
            $p_sql->execute();
            return $p_sql->fetchAll(PDO::FETCH_ASSOC);
        } catch (Exception $ex) {
            echo '<script>alert("Erro no banco de dados . '.$ex.'")</script>';
        }
    }

}
