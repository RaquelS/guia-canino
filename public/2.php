
<?php
include './dao/NoticiaDao.php';
$dao = new NoticiaDao();
$id_pag = basename(__FILE__, '.php');
$noticia = $dao->buscaNoticia($id_pag);
?>
<html>

    <head>

        <?php
        $title = "Guia Canino";
        include './head.php';
        ?>

    </head>

    <body>
        <?php
        include './header.php';
        ?>
        <div class=" container ">

            <div class="row">
                <h2 class="tituloPagina"><?= $noticia[0]['titulo'] ?></h2>
                <hr class="divisoriaTema">
            </div>
            <div class="row">
                <h5 class="categoriaNoticia"><?php
                    if ($noticia[0]['sub_categoria']) {
                        echo $noticia[0]['sub_categoria'];
                    } else {
                        echo $noticia[0]['categoria'];
                    }
                    ?></h5>
            </div>
            <div class="row">
                <h6><?= date("d/m/Y", strtotime($noticia[0]['data_publicacao'])) ?></h6>
            </div>
            <br><br>
            <div>
                <text class="textoNoticia"> <?= $noticia[0]['conteudo'] ?></text>
            </div>
            <div class="row">
                <img class="img-responsive imagemNoticia" src="<?php
                     if ($noticia[0]['imagem']) {
                         echo $noticia[0]['imagem'];
                     } else
                         echo "../img/cachorros-muita-energia.jpg";
                     ?>">

            </div>
        </div>

        <?php
        include './footer.php';
        ?>

    </body>
</html>
