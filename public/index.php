<?php
include './dao/NoticiaDao.php';
$dao = new NoticiaDao();
$listaNoticias = $dao->buscarNoticiaHome();
?>

<html>

    <head>

        <?php
        $title = "Guia Canino";
        include './head.php';
        ?>

    </head>

    <body>

        <?php
        include './header.php';
        ?>

        <div class="row" style="margin-left: 0; margin-right: 0; ">
            <div class="container-fluid containerCarousel">

                <div id="carouselHome" class="carousel" data-ride="carousel" >

                    <ol class="carousel-indicators">
                        <li data-target="#carouselHome" data-slide-to="0" class="active"></li>
                        <li data-target="#carouselHome" data-slide-to="1"></li> 
                        <li data-target="#carouselHome" data-slide-to="2"></li>
                        <li data-target="#carouselHome" data-slide-to="3"></li>
                    </ol>

                    <div class="carousel-inner center-block">
                        <div class="item active">
                            <img class="img-responsive img-rounded" src="img/golden.jpg" alt=""  />
                        </div>
                        <div class="item">
                            <img class="img-responsive img-rounded" src="img/6468.jpg" alt=""/>
                        </div>
                        <div class="item">
                            <img class="img-responsive" src="img/golden.jpg" alt=""/>
                        </div>
                        <div class="item">
                            <img class="img-responsive" src="img/6468.jpg" alt=""/>
                        </div>
                    </div>

                    <a class="left carousel-control" href="#carouselHome" data-slide="prev">
                        <span class="glyphicon glyphicon-chevron-left"></span>
                        <span class="sr-only">Previous</span>
                    </a>

                    <a class="right carousel-control" href="#carouselHome" data-slide="next">
                        <span class="glyphicon glyphicon-chevron-right"></span>
                        <span class="sr-only">Next</span>
                    </a>
                </div>
            </div>   
        </div>

        <div class=" container ">
            <div class="row">
                <div class=" col-md-12">
                    <div class="row">
                        <h2 class="tituloPagina">Últimos Artigos</h2>
                        <hr class="divisoriaTema">
                    </div>

                    <?php foreach ($listaNoticias as $noticia) {
                        ?>
                        <div class="row col-md-12"style="margin-bottom: 20px">

                            <div class="col-md-4 col-sm-4 col-xs-4">
                                <img class="img-responsive  img-thumbnail imagemMiniatura" 
                                     src="<?php
                                     if ($noticia['imagem']) {
                                         echo $noticia['imagem'];
                                     } else
                                         echo "../img/cachorros-muita-energia.jpg";
                                     ?>" alt=""/>

                            </div>
                            <div class="col-md-8 col-sm-8 col-xs-8 col-xs-8" style="padding-left: 30px;">
                                <div class="row ">
                                    <h5 class="categoriaNoticia"><?php
                                        if ($noticia['sub_categoria']) {
                                            echo $noticia['sub_categoria'];
                                        } else {
                                            echo $noticia['categoria'];
                                        }
                                        ?>
                                    </h5>

                                </div>
                                <div class="row">
                                    <h3 class="tituloNoticia"><a href="<?=$noticia['caminho_pagina']?>"><?= $noticia['titulo'] ?></a></h3>
                                </div>
                                <div class="row">
                                    <text class="conteudoNoticia descktop"><?= substr($noticia["resumo"], 0, 150) ?>...</text>
                                </div>
                                <div>
                                    <h6 class="dataNoticia"><?php
                                        if ($noticia['data_publicacao']) {
                                            echo date("d/m/Y", strtotime($noticia['data_publicacao']));
                                        }
                                        ?>
                                    </h6>
                                </div>
                            </div>
                        </div>
                    <div class="row">
                        
                    <hr class="divisoriaNoticia">
                    </div>
                    <?php } ?>

                </div>

            </div>
        </div>




        <?php
        include './footer.php';
        ?>

    </body>

</html>
