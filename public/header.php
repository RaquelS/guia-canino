
<nav class="navbar navbar-inverse cabecalho">
    <div class="container-fluid">
        <div class="navbar-header">
            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#Navbar">
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>                        
            </button>
            <img class="logo img-responsive" src="img/g1539.png" alt=""/>
        </div>
        <div class="collapse navbar-collapse"  id="Navbar" style=" border: none;">
            <ul class="nav navbar-nav" id="menu" >
                <li class="ativo" >
                    <a class="activeNavbar" href="index.php" >HOME</a>
                </li>
                <li class="dropdown">
                    <a class="dropdown-toggle" data-toggle="dropdown" href="saude.php">SAÚDE
                        <span class="caret"></span>
                    </a>
                    <ul class="dropdown-menu" style="padding: 0">
                        <li> 
                            <a href="alimentacao.php">ALIMENTAÇÃO</a>
                        </li>         
                    </ul>
                </li>
                <li class="dropdown " >
                    <a class="dropdown-toggle ativo" data-toggle="dropdown" href="comportamento.php">COMPORTAMENTO
                        <span class="caret"></span>
                    </a>
                    <ul class="dropdown-menu" style="padding: 0">
                        <li> 
                            <a href="adestramento.php">ADESTRAMENTO</a>
                        </li>

                    </ul>
                </li>
                <li><a href="cadastrar.php">CADASTRAR NOTÍCIA</a></li>
            </ul>
            
        </div>
    </div>
</nav>


